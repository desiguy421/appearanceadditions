//
//  UIBarButtonItem+AFGAppearanceAdditions.m
//  AppearanceAdditions
//
//  Created by Aqeel Gunja on 5/8/13.
//  Copyright (c) 2013 Aqeel Gunja. All rights reserved.
//

#import "UIBarButtonItem+AFGAppearanceAdditions.h"
#import "AFGAppearanceProxy.h"

@implementation UIBarButtonItem (AFGAppearanceAdditions)

+ (NSMutableDictionary *)proxyDictionary
{
    static NSMutableDictionary *proxyDictionary = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        proxyDictionary = [[NSMutableDictionary alloc] init];
    });
    return proxyDictionary;
}

+ (id)afg_appearanceForIdentifier:(NSString *)identifier
{
    AFGAppearanceProxy *proxy = [[self proxyDictionary] objectForKey:identifier];
    if (!proxy)
    {
        proxy = [[AFGAppearanceProxy alloc] initWithClass:[self class]];
        [[self proxyDictionary] setObject:proxy forKey:identifier];
    }
    return proxy;
}

+ (instancetype)afg_barButtonItemForAppearanceIdentifier:(NSString *)appearanceIdentifier
{
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] init];
    [self afg_invokeOnTarget:barButtonItem withAppearanceIdentifier:appearanceIdentifier];
    return barButtonItem;
}

+ (void)afg_invokeOnTarget:(id)target withAppearanceIdentifier:(NSString *)appearanceIdentifier
{
    AFGAppearanceProxy *proxy = [[self class] afg_appearanceForIdentifier:appearanceIdentifier];
    [proxy invokeOnTarget:target];
}

- (instancetype)initWithBarButtonSystemItem:(UIBarButtonSystemItem)systemItem target:(id)target action:(SEL)action afg_appearanceIdentifier:(NSString *)appearanceIdentifier;
{
    if ((self = [self initWithBarButtonSystemItem:systemItem target:target action:action]))
    {
        [[self class] afg_invokeOnTarget:self withAppearanceIdentifier:appearanceIdentifier];
    }
    return self;
}

@end
