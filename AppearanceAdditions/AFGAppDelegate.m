//
//  AFGAppDelegate.m
//  AppearanceAdditions
//
//  Created by Aqeel Gunja on 5/8/13.
//  Copyright (c) 2013 Aqeel Gunja. All rights reserved.
//

#import "AFGAppDelegate.h"

#import "AFGViewController.h"
#import "UIBarButtonItem+AFGAppearanceAdditions.h"

@implementation AFGAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [self styleApplication];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        self.viewController = [[AFGViewController alloc] initWithNibName:@"AFGViewController_iPhone" bundle:nil];
    } else {
        self.viewController = [[AFGViewController alloc] initWithNibName:@"AFGViewController_iPad" bundle:nil];
    }
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:self.viewController];
    self.window.rootViewController = navigationController;
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)styleApplication
{
    UIBarButtonItem *barButtonItemAppearance = [UIBarButtonItem afg_appearanceForIdentifier:@"GreenButton"];
    [barButtonItemAppearance setTintColor:[UIColor greenColor]];
    
    UIBarButtonItem *redAppearance = [UIBarButtonItem afg_appearanceForIdentifier:@"RedButton"];
    [redAppearance setTintColor:[UIColor redColor]];
    [redAppearance setTitlePositionAdjustment:UIOffsetMake(2.0, 2.0) forBarMetrics:UIBarMetricsDefault];
}

@end
