//
//  main.m
//  AppearanceAdditions
//
//  Created by Aqeel Gunja on 5/8/13.
//  Copyright (c) 2013 Aqeel Gunja. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AFGAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AFGAppDelegate class]));
    }
}
