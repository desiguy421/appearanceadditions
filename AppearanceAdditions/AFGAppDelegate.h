//
//  AFGAppDelegate.h
//  AppearanceAdditions
//
//  Created by Aqeel Gunja on 5/8/13.
//  Copyright (c) 2013 Aqeel Gunja. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AFGViewController;

@interface AFGAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) AFGViewController *viewController;

@end
