//
//  AFGAppearanceProxy.m
//  AppearanceAdditions
//
//  Created by Aqeel Gunja on 5/8/13.
//  Copyright (c) 2013 Aqeel Gunja. All rights reserved.
//

#import "AFGAppearanceProxy.h"

@interface AFGWeakReferenceContainer : NSObject

@property (nonatomic, weak) id weakObject;

@end

@implementation AFGWeakReferenceContainer
{
    NSUInteger _hashValue;
}

- (void)setWeakObject:(id)weakObject
{
    _weakObject = weakObject;
    _hashValue = [_weakObject hash];
}

- (BOOL)isEqual:(id)object
{
    return [self.weakObject isEqual:object];
}

- (NSUInteger)hash
{
    return _hashValue;
}

@end

@interface AFGAppearanceProxy ()

@property (nonatomic, strong) NSMutableArray *invocationArray;
@property (nonatomic, strong) NSMutableSet *instanceSet;

@property (nonatomic, assign) Class class;

@end

@implementation AFGAppearanceProxy

- (id)initWithClass:(Class)class
{
    if (self)
    {
        _class = class;
        _invocationArray = [[NSMutableArray alloc] init];
        _instanceSet = [[NSMutableSet alloc] init];
    }
    return self;
}

- (void)invokeOnTarget:(id)target
{
    if ([target isKindOfClass:self.class])
    {
        AFGWeakReferenceContainer *weakReferenceContainer = [[AFGWeakReferenceContainer alloc] init];
        weakReferenceContainer.weakObject = target;
        [self.instanceSet addObject:weakReferenceContainer];
        
        [self.invocationArray makeObjectsPerformSelector:@selector(invokeWithTarget:) withObject:target];
    }
}

- (NSMethodSignature *)methodSignatureForSelector:(SEL)sel
{
    return [self.class instanceMethodSignatureForSelector:sel];
}

- (void)forwardInvocation:(NSInvocation *)invocation
{
    SEL selector = [invocation selector];
    if ([self.class instancesRespondToSelector:selector])
    {
        [invocation retainArguments];
        NSInvocation *invocationToReplace = nil;
        NSInteger ndx = 0;
        
        for (NSInvocation *existingInvocation in self.invocationArray)
        {
            if ([invocation selector] == [existingInvocation selector])
            {
                invocationToReplace = existingInvocation;
                break;
            }
            ndx++;
        }
        
        if (!invocationToReplace)
        {
            [self.invocationArray addObject:invocation];
        }
        else
        {
            [self.invocationArray replaceObjectAtIndex:ndx withObject:invocation];
        }
        
        for (AFGWeakReferenceContainer *weakReferenceContainer in self.instanceSet)
        {
            id object = weakReferenceContainer.weakObject;
            if (object)
            {
                [invocation invokeWithTarget:object];
            }
        }
    }
}

@end
