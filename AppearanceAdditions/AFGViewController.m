//
//  AFGViewController.m
//  AppearanceAdditions
//
//  Created by Aqeel Gunja on 5/8/13.
//  Copyright (c) 2013 Aqeel Gunja. All rights reserved.
//

#import "AFGViewController.h"
#import "UIBarButtonItem+AFGAppearanceAdditions.h"

@interface AFGViewController ()

@end

@implementation AFGViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *greenButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:nil action:NULL afg_appearanceIdentifier:@"GreenButton"];
    UIBarButtonItem *redButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:nil action:NULL afg_appearanceIdentifier:@"RedButton"];
    UIBarButtonItem *redButton2 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:nil action:NULL afg_appearanceIdentifier:@"RedButton"];
    
    self.navigationItem.rightBarButtonItems = @[redButton, greenButton, redButton2];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)changeRedTapped:(id)sender
{
    UIBarButtonItem *redAppearance = [UIBarButtonItem afg_appearanceForIdentifier:@"RedButton"];
    [redAppearance setTitlePositionAdjustment:UIOffsetMake(3.0, 3.0) forBarMetrics:UIBarMetricsDefault];
    [redAppearance setTintColor:[UIColor cyanColor]];
}

- (IBAction)changeGreenTapped:(id)sender
{
    UIBarButtonItem *greenAppearance = [UIBarButtonItem afg_appearanceForIdentifier:@"GreenButton"];
    [greenAppearance setTitlePositionAdjustment:UIOffsetMake(-3.0, -3.0) forBarMetrics:UIBarMetricsDefault];
    [greenAppearance setTintColor:[UIColor yellowColor]];
}

@end
