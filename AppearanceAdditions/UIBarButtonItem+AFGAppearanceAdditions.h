//
//  UIBarButtonItem+AFGAppearanceAdditions.h
//  AppearanceAdditions
//
//  Created by Aqeel Gunja on 5/8/13.
//  Copyright (c) 2013 Aqeel Gunja. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFGAppearance.h"

@interface UIBarButtonItem (AFGAppearanceAdditions)
<
    AFGAppearance
>

- (instancetype)initWithBarButtonSystemItem:(UIBarButtonSystemItem)systemItem target:(id)target action:(SEL)action afg_appearanceIdentifier:(NSString *)appearanceIdentifier;

+ (instancetype)afg_barButtonItemForAppearanceIdentifier:(NSString *)appearanceIdentifier;

@end
