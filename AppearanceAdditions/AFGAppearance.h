//
//  AFGAppearance.h
//  AppearanceAdditions
//
//  Created by Aqeel Gunja on 5/8/13.
//  Copyright (c) 2013 Aqeel Gunja. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol AFGAppearance <UIAppearance>

+ (id)afg_appearanceForIdentifier:(NSString *)identifier;

@end
