//
//  AFGAppearanceProxy.h
//  AppearanceAdditions
//
//  Created by Aqeel Gunja on 5/8/13.
//  Copyright (c) 2013 Aqeel Gunja. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AFGAppearanceProxy : NSProxy

- (id)initWithClass:(Class)class;

- (void)invokeOnTarget:(id)target;

@end
